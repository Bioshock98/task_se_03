# Task Manager 3.0
## software
+ JRE
+ Java8
+ Maven 4.0
## developer
Pavel Pyshinskiy

+ email: pavel.pyshinskiy@gmail.com
## build application
```
mvn clean
mvn install
```
## run application
```
java -jar target/artifactId-version-SNAPSHOT.jar
```


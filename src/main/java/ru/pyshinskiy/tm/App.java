package ru.pyshinskiy.tm;

import ru.pyshinskiy.tm.enumerated.TerminalCommand;
import ru.pyshinskiy.tm.command.CommandExecutor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    public static void main(String[] args) throws IOException {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        CommandExecutor commandExecutor = new CommandExecutor(input);

        while(true) {
            TerminalCommand terminalCommand = TerminalCommand.EMPTY;
            String commandInput = input.readLine();
            try {
                terminalCommand = TerminalCommand.valueOf(commandInput.toUpperCase());
            }
            catch(IllegalArgumentException e) {
                System.out.println("UNKNOW_COMMAND");
                continue;
            }
            if(TerminalCommand.EXIT.equals(terminalCommand)) {
                input.close();
                System.exit(1);
            }
            commandExecutor.processCommand(terminalCommand);
        }
    }
}

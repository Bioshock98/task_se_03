package ru.pyshinskiy.tm.enumerated;

public enum TerminalCommand {
    EMPTY,
    PROJECT_CLEAR,
    PROJECT_CREATE,
    PROJECT_LIST,
    PROJECT_EDIT,
    PROJECT_SELECT,
    PROJECT_REMOVE,
    TASK_CLEAR,
    TASK_CREATE,
    TASK_LIST,
    TASK_EDIT,
    TASK_SELECT,
    TASK_SHOW_BY_PROJECT,
    TASK_ATTACH,
    TASK_UNATTACH,
    TASK_REMOVE,
    HELP,
    UNKNOW_COMMAND,
    EXIT
}

package ru.pyshinskiy.tm.manager;

import ru.pyshinskiy.tm.entity.Project;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.util.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.DateUtil.parseDateToString;

public class ProjectManager {
    private final List<Project> projects = new LinkedList<>();
    private final BufferedReader input;

    public ProjectManager(BufferedReader input) {
        this.input = input;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public Project getProject(int projectId) {
        return projects.get(projectId);
    }

    public void clearProjects() {
        projects.clear();
        System.out.println("[ALL PROJECT REMOVED]");
    }

    public void createProject() throws IOException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME");
        Project project = new Project(input.readLine());
        System.out.println("ENTER DESCRIPTION");
        project.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        project.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        project.setEndDate(parseDateFromString(input.readLine()));
        projects.add(project);
        System.out.println("[OK]");
    }

    public void listProjects(){
        System.out.println("[PROJECT LIST]");
        printProjects(projects);
        System.out.println("[OK]");
    }

    public void selectProject() throws IOException {
        System.out.println("[PROJECT SELECT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projects);
        final int projectId = Integer.parseInt(input.readLine()) - 1;
        printProject(projects.get(projectId));
        System.out.println("[OK]");
    }

    public void editProject() throws IOException {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projects);
        final int projectId = Integer.parseInt(input.readLine()) - 1;
        Project project = projects.get(projectId);
        System.out.println("ENTER NAME");
        project.setName(input.readLine());
        System.out.println("ENTER DESCRIPTION");
        project.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        project.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        project.setEndDate(parseDateFromString(input.readLine()));
        System.out.println("[OK]");
    }

    public void removeProject() throws IOException {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER PROJECT'S ID");
        printProjects(projects);
        final int projectId = Integer.parseInt(input.readLine()) - 1;
        projects.remove(projectId);
        System.out.println("[PROJECT REMOVED]");
    }

    public static void printProjects(List<Project> projects) {
        for (int i = 0; i < projects.size(); i++) {
            final Project project = projects.get(i);
            System.out.println((i + 1) + "." + " " + project.getName());
        }
    }

    private static void printProject(Project project) {
        StringBuilder formatedProject = new StringBuilder();
        formatedProject.append("project name: ");
        formatedProject.append(project.getName());
        formatedProject.append("\nproject description: ");
        formatedProject.append(project.getDescription());
        formatedProject.append("\nstart date: ");
        formatedProject.append(parseDateToString(project.getStartDate()));
        formatedProject.append("\nend date: ");
        formatedProject.append(parseDateToString(project.getEndDate()));
        System.out.println(formatedProject);
    }
}

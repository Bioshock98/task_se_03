package ru.pyshinskiy.tm.manager;

import ru.pyshinskiy.tm.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static ru.pyshinskiy.tm.manager.ProjectManager.printProjects;
import static ru.pyshinskiy.tm.util.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.DateUtil.parseDateToString;

public class TaskManager {
    private final List<Task> tasks = new LinkedList<>();
    private final ProjectManager projectManager;
    private final BufferedReader input;

    public TaskManager(ProjectManager projectManager, BufferedReader input) {
        this.projectManager = projectManager;
        this.input = input;
    }

    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        tasks.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public void createTask() throws IOException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME");
        Task task = new Task(input.readLine());
        System.out.println("ENTER DESCRIPTION");
        task.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        task.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        task.setEndDate(parseDateFromString(input.readLine()));
        tasks.add(task);
        System.out.println("[OK]");
    }

    public void listTask() {
        System.out.println("[TASK LIST]");
        printTasks(tasks);
        System.out.println("[OK]");
    }

    public void selectTask() throws IOException {
        System.out.println("[TASK SELECT");
        System.out.println("ENTER TASK ID");
        printTasks(tasks);
        final int taskId = Integer.parseInt(input.readLine()) - 1;
        Task task = tasks.get(taskId);
        printTask(task);
        System.out.println("[OK]");
    }

    public void showTasksByProject() throws IOException {
        System.out.println("[TASK SHOW BY PROJECT");
        System.out.println("ENTER PROJECT ID");
        final int projectId = Integer.parseInt(input.readLine()) - 1;
        String projectUUID = projectManager.getProject(projectId).getId();
        List<Task> tasksByProject = new LinkedList<>();
        for(Task task : tasks) {
            if(task.getProjectId().equals(projectUUID)) tasksByProject.add(task);
        }
        printTasks(tasksByProject);
        System.out.println("[OK]");
    }

    public void editTask() throws IOException {
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID");
        printTasks(tasks);
        final int taskId = Integer.parseInt(input.readLine()) - 1;
        System.out.println("ENTER NAME");
        Task task = tasks.get(taskId);
        task.setName(input.readLine());
        System.out.println("ENTER DESCRIPTION");
        task.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        task.setStartDate(parseDateFromString(input.readLine()));
        System.out.println("ENTER END DATE");
        task.setEndDate(parseDateFromString(input.readLine()));
        System.out.println("[OK]");
    }

    public void attachTask() throws IOException {
        System.out.println("[TASK ATTACH");
        System.out.println("ENTER TASK ID");
        printTasks(tasks);
        final int taskId = Integer.parseInt(input.readLine()) - 1;
        Task task = tasks.get(taskId);
        System.out.println("ENTER PROJECT ID");
        printProjects(projectManager.getProjects());
        final int projectId = Integer.parseInt(input.readLine()) - 1;
        String projectUUID = projectManager.getProject(projectId).getId();
        task.setProjectId(projectUUID);
        System.out.println("[OK]");
    }

    public void unattachTask() throws IOException {
        System.out.println("[TASK UNATTACH");
        System.out.println("ENTER TASK ID");
        printTasks(tasks);
        final int taskId = Integer.parseInt(input.readLine()) - 1;
        Task task = tasks.get(taskId);
        task.setProjectId(null);
        System.out.println("[OK]");
    }

    public void removeTask() throws IOException {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER TASK ID");
        printTasks(tasks);
        final int taskId = Integer.parseInt(input.readLine()) - 1;
        tasks.remove(taskId);
        System.out.println("[TASK REMOVED]");
    }

    private static void printTasks(List<Task> tasks) {
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println((i + 1) + "." + " " + tasks.get(i).getName());
        }
    }

    private void printTask(Task task) {
        StringBuilder formatedTask = new StringBuilder();
        formatedTask.append("task name: ");
        formatedTask.append(task.getName());
        formatedTask.append("\ntask description: ");
        formatedTask.append(task.getDescription());
        formatedTask.append("\nstart date: ");
        formatedTask.append(parseDateToString(task.getStartDate()));
        formatedTask.append("\nend date: ");
        formatedTask.append(parseDateToString(task.getEndDate()));
        System.out.println(formatedTask);
    }
}

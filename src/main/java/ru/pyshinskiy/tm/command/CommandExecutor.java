package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.enumerated.TerminalCommand;
import ru.pyshinskiy.tm.manager.ProjectManager;
import ru.pyshinskiy.tm.manager.TaskManager;

import java.io.BufferedReader;
import java.io.IOException;

public class CommandExecutor {
    private ProjectManager projectManager;
    private TaskManager taskManager;

    public CommandExecutor(BufferedReader input) {
        this.projectManager = new ProjectManager(input);
        this.taskManager = new TaskManager(projectManager, input);
    }

    public void processCommand(TerminalCommand terminalCommand) throws IOException {
        switch (terminalCommand) {
            case PROJECT_CLEAR:
                projectManager.clearProjects();
                break;
            case PROJECT_CREATE:
                projectManager.createProject();
                break;
            case PROJECT_LIST:
                projectManager.listProjects();
                break;
            case PROJECT_SELECT:
                projectManager.selectProject();
                break;
            case PROJECT_EDIT:
                projectManager.editProject();
                break;
            case PROJECT_REMOVE:
                projectManager.removeProject();
                break;
            case TASK_CLEAR:
                taskManager.clearTasks();
                break;
            case TASK_CREATE:
                taskManager.createTask();
                break;
            case TASK_LIST:
                taskManager.listTask();
                break;
            case TASK_SELECT:
                taskManager.selectTask();
                break;
            case TASK_SHOW_BY_PROJECT:
                taskManager.showTasksByProject();
                break;
            case TASK_ATTACH:
                taskManager.attachTask();
                break;
            case TASK_UNATTACH:
                taskManager.unattachTask();
                break;
            case TASK_EDIT:
                taskManager.editTask();
                break;
            case TASK_REMOVE:
                taskManager.removeTask();
                break;
            case HELP:
                help();
                break;
        }
    }

    private void help() {
        StringBuilder help = new StringBuilder();
        help
                .append("help: Show all commands.\n")
                .append("project_clear: Remove all projects.\n")
                .append("project_create: Create new project.\n")
                .append("project_list: Show all projects.\n")
                .append("project_edit: Edit selected project\n")
                .append("project_remove: Remove selected project\n")
                .append("task_clear: Remove all tasks.\n")
                .append("task_create: Create new task.\n")
                .append("task_list: Show all tasks.\n")
                .append("task_edit: Edit selected task.\n")
                .append("task_remove: Remove selected task.");
        System.out.println(help);
    }
}
